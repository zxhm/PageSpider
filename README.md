# 页面爬虫工具

一个简单的页面爬虫工具，适用爬取不太复杂的网页中的图片和文本。

## 技术

- .Net Framework 4.5.2
- .CefSharp 67 程序中的浏览器控件
- .AngleSharp Dom解析

## 参数说明：

- 主页URL：要爬取的主页面地址，必填
- 主页选择器：主页面中要爬去取的内容的选择器或者导航到子页面的链接的选择器，必填
- 标题选择器：主页面中要爬取的内容的标题或者导航到子页面的标题要素选择器，必填
- 下页选择器：主页面中下一页链接的选择器，与主页翻页数配合使用
- 次页选择器：子页面中要抓取的内容的选择器，如果有次页选择器，则主页选择器只是链接，不抓取内容
- 次标选择器：子页面中标题要素的选择器
- 次下选择器：子页面中下一页链接的选择器
- 文件名前缀：抓取的图片或者txt文件的文件名前缀
- 保存文件夹：抓取的文件保存的根文件夹
- 主页翻页数：与下页选择器配合使用，如果翻页数是1，则不需要下页选择器

## 示例

### 发现中国

- 运行参数：
![image](Image/示例-发现中国1.png)  
- 结果文件夹：
![image](Image/示例-发现中国2.png) 
- 结果文件：
![!image](Image/示例-发现中国3.png)

### 小众软件

- 运行参数：
![image](Image/示例-小众软件1.png)  
- 结果文件夹：
![image](Image/示例-小众软件2.png)  
- 结果文件：
![image](Image/示例-小众软件3.png)

### 妹子图

- 运行参数：
![image](Image/示例-妹子图1.png)  
- 结果文件夹：
![image](Image/示例-妹子图2.png)  
- 结果文件：
![image](Image/示例-妹子图3.png)

## 待完善

- [ ] 软件只支持结构比较简单的页面抓取，太复杂的会出问题
- [ ] 代码目前做的错误处理不完善
- [ ] html内容转成文本文件时对html标签特别是js代码的处理不够干净