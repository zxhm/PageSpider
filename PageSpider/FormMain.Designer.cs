﻿namespace PageSpider
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btn_go = new System.Windows.Forms.Button();
            this.btn_folder = new System.Windows.Forms.Button();
            this.btn_start = new System.Windows.Forms.Button();
            this.tb_folder = new System.Windows.Forms.TextBox();
            this.tb_page_count = new System.Windows.Forms.TextBox();
            this.tb_prefix = new System.Windows.Forms.TextBox();
            this.tb_sub_next_sel = new System.Windows.Forms.TextBox();
            this.tb_sub_title_sel = new System.Windows.Forms.TextBox();
            this.tb_sub_sel = new System.Windows.Forms.TextBox();
            this.tb_next_sel = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_title_sel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_sel = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_url = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_log = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp_main = new System.Windows.Forms.TabPage();
            this.tp_sub = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(1181, 635);
            this.splitContainer1.SplitterDistance = 257;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.btn_stop);
            this.splitContainer2.Panel1.Controls.Add(this.btn_go);
            this.splitContainer2.Panel1.Controls.Add(this.btn_folder);
            this.splitContainer2.Panel1.Controls.Add(this.btn_start);
            this.splitContainer2.Panel1.Controls.Add(this.tb_folder);
            this.splitContainer2.Panel1.Controls.Add(this.tb_page_count);
            this.splitContainer2.Panel1.Controls.Add(this.tb_prefix);
            this.splitContainer2.Panel1.Controls.Add(this.tb_sub_next_sel);
            this.splitContainer2.Panel1.Controls.Add(this.tb_sub_title_sel);
            this.splitContainer2.Panel1.Controls.Add(this.tb_sub_sel);
            this.splitContainer2.Panel1.Controls.Add(this.tb_next_sel);
            this.splitContainer2.Panel1.Controls.Add(this.label8);
            this.splitContainer2.Panel1.Controls.Add(this.label9);
            this.splitContainer2.Panel1.Controls.Add(this.tb_title_sel);
            this.splitContainer2.Panel1.Controls.Add(this.label7);
            this.splitContainer2.Panel1.Controls.Add(this.tb_sel);
            this.splitContainer2.Panel1.Controls.Add(this.label10);
            this.splitContainer2.Panel1.Controls.Add(this.label5);
            this.splitContainer2.Panel1.Controls.Add(this.label4);
            this.splitContainer2.Panel1.Controls.Add(this.label6);
            this.splitContainer2.Panel1.Controls.Add(this.label3);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.tb_url);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tb_log);
            this.splitContainer2.Size = new System.Drawing.Size(257, 635);
            this.splitContainer2.SplitterDistance = 400;
            this.splitContainer2.TabIndex = 0;
            // 
            // btn_stop
            // 
            this.btn_stop.Location = new System.Drawing.Point(146, 366);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(75, 23);
            this.btn_stop.TabIndex = 25;
            this.btn_stop.Text = "停止";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btn_go
            // 
            this.btn_go.Location = new System.Drawing.Point(212, 15);
            this.btn_go.Name = "btn_go";
            this.btn_go.Size = new System.Drawing.Size(33, 23);
            this.btn_go.TabIndex = 24;
            this.btn_go.Text = "→";
            this.btn_go.UseVisualStyleBackColor = true;
            this.btn_go.Click += new System.EventHandler(this.btn_go_Click);
            // 
            // btn_folder
            // 
            this.btn_folder.Location = new System.Drawing.Point(212, 295);
            this.btn_folder.Name = "btn_folder";
            this.btn_folder.Size = new System.Drawing.Size(33, 23);
            this.btn_folder.TabIndex = 24;
            this.btn_folder.Text = "...";
            this.btn_folder.UseVisualStyleBackColor = true;
            this.btn_folder.Click += new System.EventHandler(this.btn_folder_Click);
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(29, 366);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 23);
            this.btn_start.TabIndex = 23;
            this.btn_start.Text = "开始";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // tb_folder
            // 
            this.tb_folder.Location = new System.Drawing.Point(84, 296);
            this.tb_folder.Name = "tb_folder";
            this.tb_folder.ReadOnly = true;
            this.tb_folder.Size = new System.Drawing.Size(122, 21);
            this.tb_folder.TabIndex = 6;
            // 
            // tb_page_count
            // 
            this.tb_page_count.Location = new System.Drawing.Point(84, 331);
            this.tb_page_count.Name = "tb_page_count";
            this.tb_page_count.Size = new System.Drawing.Size(161, 21);
            this.tb_page_count.TabIndex = 7;
            this.tb_page_count.Text = "1";
            // 
            // tb_prefix
            // 
            this.tb_prefix.Location = new System.Drawing.Point(84, 261);
            this.tb_prefix.Name = "tb_prefix";
            this.tb_prefix.Size = new System.Drawing.Size(161, 21);
            this.tb_prefix.TabIndex = 8;
            // 
            // tb_sub_next_sel
            // 
            this.tb_sub_next_sel.Location = new System.Drawing.Point(84, 226);
            this.tb_sub_next_sel.Name = "tb_sub_next_sel";
            this.tb_sub_next_sel.Size = new System.Drawing.Size(161, 21);
            this.tb_sub_next_sel.TabIndex = 9;
            // 
            // tb_sub_title_sel
            // 
            this.tb_sub_title_sel.Location = new System.Drawing.Point(84, 191);
            this.tb_sub_title_sel.Name = "tb_sub_title_sel";
            this.tb_sub_title_sel.Size = new System.Drawing.Size(161, 21);
            this.tb_sub_title_sel.TabIndex = 10;
            // 
            // tb_sub_sel
            // 
            this.tb_sub_sel.Location = new System.Drawing.Point(84, 156);
            this.tb_sub_sel.Name = "tb_sub_sel";
            this.tb_sub_sel.Size = new System.Drawing.Size(161, 21);
            this.tb_sub_sel.TabIndex = 10;
            // 
            // tb_next_sel
            // 
            this.tb_next_sel.Location = new System.Drawing.Point(84, 121);
            this.tb_next_sel.Name = "tb_next_sel";
            this.tb_next_sel.Size = new System.Drawing.Size(161, 21);
            this.tb_next_sel.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 300);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 12);
            this.label8.TabIndex = 21;
            this.label8.Text = "保存文件夹：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 335);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 19;
            this.label9.Text = "主页翻页数：";
            // 
            // tb_title_sel
            // 
            this.tb_title_sel.Location = new System.Drawing.Point(84, 86);
            this.tb_title_sel.Name = "tb_title_sel";
            this.tb_title_sel.Size = new System.Drawing.Size(161, 21);
            this.tb_title_sel.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 265);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 20;
            this.label7.Text = "文件名前缀：";
            // 
            // tb_sel
            // 
            this.tb_sel.Location = new System.Drawing.Point(84, 51);
            this.tb_sel.Name = "tb_sel";
            this.tb_sel.Size = new System.Drawing.Size(161, 21);
            this.tb_sel.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 195);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 18;
            this.label10.Text = "次标选择器：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 22;
            this.label5.Text = "次下选择器：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 18;
            this.label4.Text = "次页选择器：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 17;
            this.label6.Text = "标题选择器：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 16;
            this.label3.Text = "下页选择器：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 15;
            this.label2.Text = "主页选择器：";
            // 
            // tb_url
            // 
            this.tb_url.Location = new System.Drawing.Point(84, 16);
            this.tb_url.Name = "tb_url";
            this.tb_url.Size = new System.Drawing.Size(122, 21);
            this.tb_url.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "主  页 URL：";
            // 
            // tb_log
            // 
            this.tb_log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_log.Location = new System.Drawing.Point(0, 0);
            this.tb_log.Multiline = true;
            this.tb_log.Name = "tb_log";
            this.tb_log.ReadOnly = true;
            this.tb_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_log.Size = new System.Drawing.Size(257, 231);
            this.tb_log.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tp_main);
            this.tabControl1.Controls.Add(this.tp_sub);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(920, 635);
            this.tabControl1.TabIndex = 0;
            // 
            // tp_main
            // 
            this.tp_main.Location = new System.Drawing.Point(4, 22);
            this.tp_main.Name = "tp_main";
            this.tp_main.Padding = new System.Windows.Forms.Padding(3);
            this.tp_main.Size = new System.Drawing.Size(912, 609);
            this.tp_main.TabIndex = 0;
            this.tp_main.Text = "主页";
            this.tp_main.UseVisualStyleBackColor = true;
            // 
            // tp_sub
            // 
            this.tp_sub.Location = new System.Drawing.Point(4, 22);
            this.tp_sub.Name = "tp_sub";
            this.tp_sub.Padding = new System.Windows.Forms.Padding(3);
            this.tp_sub.Size = new System.Drawing.Size(912, 609);
            this.tp_sub.TabIndex = 1;
            this.tp_sub.Text = "次页";
            this.tp_sub.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1181, 635);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormMain";
            this.Text = "页面爬虫";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tp_main;
        private System.Windows.Forms.TabPage tp_sub;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Button btn_folder;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.TextBox tb_folder;
        private System.Windows.Forms.TextBox tb_page_count;
        private System.Windows.Forms.TextBox tb_prefix;
        private System.Windows.Forms.TextBox tb_sub_next_sel;
        private System.Windows.Forms.TextBox tb_sub_sel;
        private System.Windows.Forms.TextBox tb_next_sel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_title_sel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_sel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_url;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_log;
        private System.Windows.Forms.Button btn_go;
        private System.Windows.Forms.TextBox tb_sub_title_sel;
        private System.Windows.Forms.Label label10;
    }
}

